## App Engine

### Deploying an app to app engine

Run the following command in the app engine folder

``` 
gcloud app deploy
```

### command to install app engine python libraries in the lib folder

#### Windows
Run the following command in the app engine folder

```
python -m pip install -r requirements.txt -t lib\
```

### Command to modify object change notifications

#### Add object change notification

Remember to save the channel and resource id from the response so that you can later remove the channel. If you fail to do this you can always get the channel and resource id from the logs in stackdriver for the app engine app
```
gsutil notification watchbucket https://gcs-to-bq-dot-scg-dai-sci-dev.appspot.com/<bigquery_dataset_id> gs://<gcs_bucket_id>/
```

#### Remove object change notification

```
gsutil notification stopchannel 0cd419cc-9b2d-4c66-ab6e-79ab2b6d1566 _8QcxFsj62txT4ex5zSKxiToGvE
```

