#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import os
import json
import googleapiclient.discovery
from jinja2 import Environment, FileSystemLoader
import logging
from uuid import uuid4
import cloudstorage as gcs
from cloudstorage import errors
from google.appengine.api import app_identity
import re

# Create the bigquery api client
bq = googleapiclient.discovery.build('bigquery', 'v2')

env = Environment(
    loader=FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

template = env.get_template('bq_load.json')


class MainHandler(webapp2.RequestHandler):

    def post(self, dataset_id):
        logging.debug(self.request)
        try:
            req = json.loads(self.request.body)
            project_id = app_identity.get_application_id()

            # Only process if this is not a schema file
            if not (os.path.splitext(req["name"])[1] == ".schema"):
                source_uri = "gs://%s/%s" % (req["bucket"], req["name"])
                table_path = os.path.splitext(req["name"])[0]
                schema_uri = "/%s/%s.schema" % (req["bucket"], table_path)
                table_id = re.sub('[^A-Za-z0-9_]+', '_', table_path)

                # debug logging
                logging.debug('project_id: %s' % project_id)
                logging.debug('source_uri: %s' % source_uri)
                logging.debug('table_path: %s' % table_path)
                logging.debug('schema_uri: %s' % schema_uri)
                logging.debug('dataset_id: %s' % dataset_id)
                logging.debug('table_id: %s' % table_id)

                config = json.loads(template.render(project_id=project_id,
                                                    job_id=str(uuid4()),
                                                    source_uri=source_uri,
                                                    dataset_id=dataset_id,
                                                    table_id=table_id))
                try:
                    gcs_file = gcs.open(schema_uri)
                    schema_content = gcs_file.read()
                    gcs_file.close()
                except errors.NotFoundError:
                    schema_content = ""

                if schema_content:
                    config["configuration"]["load"]["schema"] = json.loads(schema_content)
                    config["configuration"]["load"]["fieldDelimiter"] = "\t"
                else:
                    config["configuration"]["load"]["autodetect"] = True

                bq.jobs().insert(projectId=project_id, body=config).execute()

        except ValueError:
            pass


app = webapp2.WSGIApplication([
    webapp2.Route("/<dataset_id>", MainHandler)

], debug=True)
